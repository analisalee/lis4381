import java.util.Scanner;

import javax.lang.model.util.ElementScanner6;

public class DecisionStatements
{
   public static void main(String args[])
   {
        //display operational messages
        System.out.println("Program evaluates user-entered characters.");
        System.out.println("Use the following characters to: W or w, C or c, H or h, N or n");
        System.out.println("Use the following decision structures: if ... else, and switch");
        System.out.println(); //print blank line

        String myStr= "";
        char myChar= ' ';
        Scanner sc = new Scanner(System.in);

        /* 
        NOTE: no API method to get a character from the Scanner. TO fix this I must get the String using the scanner.Next() and invoke String.charAt(0) method on returned String
        */

        System.out.println("Phone types: W or w (work), C or c (cell), H or h (home), N or n (none).");
        System.out.print("Enter phone type: ");
        myStr = sc.next().toLowerCase();
        myChar = myStr.charAt(0);

        System.out.println("\nif...else:");

        if (myChar == 'w')
            System.out.println("phone type: work");
        else if (myChar == 'c')
            System.out.println("Phone type: cell");
        else if (myChar == 'h')
            System.out.println("Phone type: home");
        else if (myChar == 'n');
            System.out.println("Phone type: none");
            
        System.out.println("Switch:");
        switch (myChar)
        {
            case 'w':
                System.out.println("phone type: work");
                break;
            
            case 'c':
                System.out.println("phone type: cell");
                break;
                
            case 'h':
                System.out.println("phone type: home");
                break;
               
            case 'n':
                System.out.println("phone type: none");  
                break;
              
            default:
                System.out.println("Incorrect character entry");
                break;    
        }    


   }
}