
# LIS 4381 - Mobile Application Development and Management

## Analisa Lee

### LIS 4381 Assignment Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    * Instal AMPPS
    * Instal JDK
    * Instal Android Studio and create My First App
    * Instal Bitbucket repo
    * Create Bitbucket Tutorials 
    * Provide git command descriptions

2.  [A2 README.md](a2/README.md "My A2 README.md file")
    * used Andriod Studip to create an app
    * Changed background color of my choice
    * Provide screenshots of the user interfaces

3.  [A3 README.md](a3/README.md "My A3 README.md file")
    * used android studio to create an app that calculates the price of tickets
    * created a database using MySQL that shows different pet stores, the customers, and pets and data involving them
    * provided screenshots of database and app

4. [P1 README.md](p1/README.md "My P1 README.md file")
    * Used Android Studios to create a business card with my contact information on it.
    * Changed background color of both interfaces.
    * Created a broder around my image.

5. [A4 README.md](a4/README.md "My A4 README.md file")
    * Created a local repo to display my work in LIS4381
    * Used PHP to code

6. [A5 README.md](a5/README.md "My A5 README.md file")
    * Connected database with local repo
    * Used PHP to develop a server-side form

7. [P2 README.md](p2/README.md "My P2 README.md file")
    * Clone A5 files
    * Code using PHP
    * Connect database to local repo
    * Be able to update form with server and client side validation
    * Update data entries
    * Delete data entries
    * Create RSS Feed
    * Add image to carousel