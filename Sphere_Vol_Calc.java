import java.util.Scanner;
import java.lang.Math;

class Sphere_Volume_Calc
{
    public static void main(String args[])
    {
        System.out.println("Sphere Volume Program\n");

        //display operational messages
        System.out.println("Program calculates sphere volume in liquid US gallons from user entered diameter value in inches,\nand rounds to two decimals.");
        System.out.println("Must use Java\'s *built in* PI and pow() capabilities.");
        System.out.println("program checks for non integer and non numeric values");
        System.out.println("program continutes to prompt for user entry until no longer requested, prompt accepts upper or lower case letters");


        System.out.println(); //print blank line

        //initialize variables and create scanner object
        int diameter = 0;
        double volume = 0.0;
        double gallons = 0.0;
        char choice = ' '; 
        Scanner sc = new Scanner(System.in);

        do
        {
            System.out.print("Please enter diameter in inches: ");
            while (!sc.hasNextInt())
            {
                System.out.println("Not valid integer!\n");
                sc.next();
                System.out.print("Please try again. Enter diameter in inches: ");
            }

            diameter = sc.nextInt();

            System.out.println();

            volume = ((4.0/3.0)*Math.PI*Math.pow(diameter/2.0, 3));
            gallons = volume/231;
            System.out.println("Sphere volume: " + String.format("%,.2f",gallons) + " liquid US gallons.");

            System.out.print("\nDo you want to calculate another sphere volume (y or n?)");
            choice = sc.next().charAt(0);
            choice = Character.toLowerCase(choice);
        }
        while (choice == 'y');

        System.out.println("\nThank you for using our Sphere Volume Calculator!");

    }
}