> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Analisa Lee

### Project 1 Requirements:

*Two Parts:*

1. Create a mobile business card app using Android Studio.
2. Border around image
3. Change background color
4. Questions

#### README.md file should include the following items:

* Screenshot of running application’s first user interface
* Screenshot of running application’s second user interface
* Course title, your name, assignment requirements

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of running application’s first and second user interfaces*:

![First User interface Screenshot](IMG/first.png) ![Second user interface Screenshot](IMG/second.png)







