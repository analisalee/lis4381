> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Analisa Lee

### Assignment 3 Requirements:

*Three Parts:*

1. Created a database using MySQL
2. Developmed an app that calculates the cost per ticket
3. Questions

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running application�s first user interface
* Screenshot of running application�s second user interface
* Links to the following files:
        a. a3.mwb
        b. a3.sql

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> 

#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD Screenshot](IMG/erd.png)

*Screenshot of interfaces*:

![First Interface Screenshot](IMG/first.png) ![Second Interface Screenshot](IMG/second.png)





#### Links:

*a3.sql link:*
[a3.sql Link](a3.sql/ "a3.sql")

*a3.mwb:*
[a3.mwb Link](a3.mwb/ "a3.mwb")
