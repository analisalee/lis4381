> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Analisa Lee

### Project 2 Requirements:

*Four Parts:*

1. Clone A5 files
2. Code using PHP
3. Connect database to local repo
4. Be able to update form with server and client side validation
5. Update data entries
6. Delete data entries
7. Create RSS Feed
8. Add image to carousel
9. Questions

#### README.md file should include the following items:

* Screenshot of index.php
* Screenshot of edit_petstore.php
* Screenshot of edit_petstore_process.php (that includes error.php)
* Screenshot of Carousel with image
* Screenshot of RSS Feed
* link to repo
* Course title, your name, assignment requirements

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of running application’s first and second user interfaces*:

![Screenshot of index.php](IMG/first.png) ![Screenshot of edit_petstore.php](IMG/second.png)
![Screenshot of edit_petstore_process.php (that includes error.php)](IMG/third.png)
![Screenshot of Carousel with image](IMG/fourth.png)
![Screenshot of RSS Feed](IMG/fifth.png)



#### Links
*Link to local lis4381 web app:*
[My Online Portfolio](http://localhost/repos/lis4381/ "My online Portfolio")








