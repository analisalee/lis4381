import java.util.Scanner;

class methods
{
    public static void displayProgram()
    {
        //display operational messages
        System.out.println("Program prompts user for first namw and age, then prints results");
        System.out.println("Create three methods from the following requirements:");
        System.out.println("1. displayProgram(): void method that displays program reqgirements.");
        System.out.println("2. myVoidMethod():\n" + "\ta. Accepts two arguments: Strings and int. \n" + "\tb. Prints user's first name and age.");
        System.out.println("3. myValueReturningMethod():\n" + "\ta. Accepts two arguments: String and int. \n" + "\tb. Returns String containing first name and age.");
        System.out.println(); //print blank line

    }
    
    //note: both methods use same named parameters which are locla variables
    public static void myVoidMethod(String first, int age)
    {
        System.out.println(first + " is " + age);
    }

    public static String myValueReturningMethod(String first, int age)
    {
        return first + " is " + age;
    }

    public static void main(String args[])
    {
        //call void method: display operational messages
        displayProgram();

        //initialize variables, create Scanner object, capture user input
        String firstName="";
        int userAge = 0;
        String myStr="";
        Scanner sc = new Scanner(System.in);

        //input
        System.out.print("Enter first name: ");
        firstName=sc.next();
        
        System.out.print("Enter age: ");
        userAge = sc.nextInt();

        System.out.println(); //print blank line

        //call void method
        System.out.print("void method call: ");
        myVoidMethod(firstName, userAge);

        //call value-returning method
        System.out.print("Value-returning method call: ");
        myStr = myValueReturningMethod(firstName, userAge);
        System.out.println(myStr);
    }

}