import java.util.Scanner;
import java.util.*; //need for Array list


public class ArrayLists
{
    public static void main(String args[])
    {
        //dispay operational messages
        System.out.println("Program populates ArrayLits of strings with user entered animal type value.");
        System.out.println("Examples: polar bear, guinea pig, dog, cat, bird.");
        System.out.println("Program contibues to collect user entered values until user types\"n\".");
        System.out.println("Program displays ArrayList values after each iteration, as well as size.");

        System.out.println();

        //create program variables/objects
        //create Scanner object
        Scanner sc = new Scanner(System.in);
        ArrayList<String> obj = new ArrayList<String>(); //create String type ArrayList
        String myStr = "";
        String choice = "y";
        int num = 0;

        while (choice.equals("y"))
            {
                System.out.print("Enter animal type: ");
                myStr = sc.nextLine();
                obj.add(myStr); //add String object
                num = obj.size(); //returns ArrayList size
                System.out.println("ArrayList elements: " + obj + "\nArrayList Size = " + num);
                System.out.print("\nContinue? y or n: ");
                choice = sc.next();
                sc.nextLine();
            }
    }
}