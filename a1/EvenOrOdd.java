import java.util.Scanner;

public class EvenOrOdd
{
   public static void main(String[] args)
   {
      { 
          int x;
         System.out.println("Enter number:   ");
         Scanner in = new Scanner(System.in);
         x = in.nextInt();

         if (x % 2 == 0)
            System.out.println(x + " is an even");
         else
            System.out.println( x + " is an odd");   
      }
   }
}
