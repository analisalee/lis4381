> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Analisa Lee

### Assignment 1 Requirements:

*Six Parts:*

1. Instal AMPPS
2. Instal JDK
3. Instal Android Studio and create My First App
4. Instal Bitbucket repo
5. Create Bitbucket Tutorials
6. Provide git command descriptions

#### README.md file should include the following items:

* Screenshot of ampps installation running
* Screenshot of running JDK java Hello
* Screenshot of running Android Studio My First App

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> 

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![ERD Screenshot](IMG/ampps.png)

*Screenshot of Android Studio - My First App*

![First Interface Screenshot](IMG/android.png) 

*Screenshot of Java*:

![Second Interface Screenshot](IMG/jdk_install.png)

*Screenshot of first interface*:



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")