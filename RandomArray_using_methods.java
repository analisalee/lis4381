import java.util.Scaner;
import java.util.Random;

class RandomArray_using_methods
{
    public static void displayProgram()
    {
        System.out.println("Program prompts user to enter desired number of psuedorandom-generated integers (min 1).");
        System.out.println("Use following loop structures: for, enhanced for, while, do ... while.");
        System.out.println("Note: create *and* display array values using *at least* on valure-returning method, and one void method.");
        System.out.println(); //print blank line
    }

    public static int getArraySize()
    {
        //declare variables and create scanner object
        int arraySize;
        Scanner input = new Scanner(System.in);

        System.out.println("Enter desired number of pseudo-random integers: ");
        while (!input.hasNetInt())
            {
                System.out.println("Not valid integer!\n");
                input.next(); //important! if omitted, will go into infinite loop on invalid input
                System.out.print("Please try again. Enter Desired number of pseudo-random integers: ");
            }
        arraySize = input.nextInt();
        return arraySize;   
    }

    public static void displayArrays(int num)
    {
        Random r = new Random(); //instantiate random object variable
        int i = 0;

        //Java stsyle String[] myArray
        //C++ style String myArray[]
        int myArray[] = new int[num];
        
        System.out.println("For loop:");
        for(i=0; i< myArray.length; i++)
            {
                System.out.println(r.nextInt());
            }

        System.out.println("\nEnhanced for loop:");
        for(int n: myArray)
            {
                System.out.println(r.nextInt());
            }
         
        System.out.println("\nwhile loop:");
        i=0;
        while (i< myArray.length)
            {
                System.out.println(r.nextInt());
                i++;
            }    
        i=0;
        System.out.println("\ndo ... while loop:");
        do
            {
                System.out.println(r.nextInt());
                i++;
            }
        while (i < myArray.length);    
    }

    public static void main (Strings args[])
    {
        //call void method: display operation messages
        displayProgram();

        //call value-returning method: get array size
        int arrayNum = getArraySize();

        //call void method: print arrays
        displayArrays(arrayNum);
    }
}