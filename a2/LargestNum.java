import java.util.Scanner;

class LargestNum
{
    public static void main(String args[])
    {
        System.out.println("Program evaluates the largest of two integers.");
        System.out.println("Note: Does not check for non numeric characters or non integer values.");
        System.out.println();

        int num1, num2;
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter first number: ");
        num1 = sc.nextInt();

        System.out.print("Enter the second number: ");
        num2 = sc.nextInt();

        System.out.println();

        if (num1 > num2)
            System.out.println(num1 + " is larger than " + num2);

        else if (num2 > num1)
            System.out.println(num2 + " is larger than " + num1);

        else
            System.out.println("The numbers are equal.");

    }
}