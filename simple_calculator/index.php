]<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Simple calculator.">
	<meta name="author" content="Analisa Lee">
	<link rel="icon" href="favicon.ico">

	<title>Simple Calculator</title>
	<?php include_once("../css/include_css.php"); ?>

</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

					<h2>Perform Calculation</h2>

						<form id="add_store_form" method="post" class="form-horizontal" action="process.php">
								<div class="form-group">
										<label class="col-sm-3 control-label">Num1:</label>
										<div class="col-sm-6">
												<input type="text" class="form-control" name="num1" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-3 control-label">Num2:</label>
										<div class="col-sm-6">
												<input type="text" class="form-control" name="num2" />
										</div>
								</div>

								<label class="radio-inline"><input type="radio" name="operator" value="add">addition </label>
								<label class="radio-inline"><input type="radio" name="operator" value="subtract">subtraction </label>
								<label class="radio-inline"><input type="radio" name="operator" value="multiply">multiplication </label>
								<label class="radio-inline"><input type="radio" name="operator" value="divide">division </label>
								<label class="radio-inline"><input type="radio" name="operator" value="exponent">exponentiation </label>
								
								<p>&nbsp;</p>

								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-3">
									<button type="submit" class="btn btn-primary" name="calculate" value="Calculate">Calculate</button>
										</div>
								</div>
						</form>

			<?php include_once "global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("../js/include_js.php"); ?>

<script type="text/javascript">
 //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
 $(document).ready(function() {

	$('#add_store_form').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {
					num1: {
							validators: {
									notEmpty: {
									 		message: 'Num1 required'
									},
									stringLength: {
											min: 1,
									 		message: 'Num1 needs atleast one character'
									},
									regexp: {
										regexp: /^[0-9]+$/,
										message: 'Num1 can only contain numbers'
									},									
							},
					},

					num2: {
							validators: {
									notEmpty: {
											message: 'Num2 required'
									},
									stringLength: {
											min: 1,
									 		message: 'Num2 needs atleast one character'
									},
									regexp: {
										regexp: /^[0-9]+$/,		
										message: 'Num2 can only contain numbers'
									},									
							},
					},
					
			}
	});
});
</script>

</body>
</html>
