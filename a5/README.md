> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Analisa Lee

### Assignment 5 Requirements:

*Four Parts:*

1. Clone A4 files
2. Code using PHP
3. Connect database to local repo
4. Be able to update form with server and client side validation
5. Questions

#### README.md file should include the following items:

* Screenshot of index.php
* Screenshot of add_petstore_process.php (that includes error.php)
* link to repo
* Course title, your name, assignment requirements

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of running application’s first and second user interfaces*:

![First User interface Screenshot](IMG/first.png) ![Second user interface Screenshot](IMG/second.png)


#### Links
*Link to local lis4381 web app:*
[My Online Portfolio](http://localhost/repos/lis4381/ "My online Portfolio")








