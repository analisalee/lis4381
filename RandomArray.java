import java.util.Scanner;
import java.util.Random;

class RandomArray
{
    public static void main(String args[])
    {
        //display operational messages
        System.out.println("Program prompts user to enter desired number of psuedodorandom-generated integers (min).");
        System.out.println("Use the following loop structures: for, enhanced for, while, do...while.");
        System.out.println(); //print blank line

        Scanner sc = new Scanner(System.in);
        Random r = new Random(); //initiate random object variable
        int arraySize = 0;
        int i = 0;

        //prompt user for number of randomly generated numbers
        System.out.print("Enter desired number of pseudorandom-generated integers (min 1): ");
        arraySize = sc.nextInt();

        //Java style String[] myArray[]
        //C++ style String myArray[]
        int myArray[] = new int[arraySize];

        System.out.println("for loop: ");
        for(i=0; i< myArray.length; i++)
        {
            //nextInt(int n): pseudorandom, uniformly distributed int value between 0 (inclusive), and specific value (exclusive)
            //example: to generate numbers from min to max (including both):
            //int x = r.nextInt(max - min + 1) + min
            //int x = r.nextInt(10) + 1; //generate number between 1 and 10, inclusive
            System.out.println(r.nextInt());
            //System.out.println(r.nextInt(10) + 1); //print pseudorandom number between 1 and 10, inclusive
        }

        System.out.println("\nEnhanced for loop:");
        for(int n: myArray)
        {
            System.out.println(r.nextInt());
        }

        System.out.println("\nWhile loop:");
        i = 0; //reassign to 0
        while(i < myArray.length)
        {
            System.out.println(r.nextInt());
            i++;
        }

        i=0; //reassign to 0
        System.out.println("\ndo...while loop:");
        do
        {
            System.out.println(r.nextInt());
            i++;
        }
        while(i < myArray.length);
    }
}