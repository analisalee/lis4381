import java.util.Scanner;
import java.util.Arrays;

public class ArrayRuntimeNum 
{
    public static void main (String[] args)
    {
        int size=0;
        double sum=0;
        double average=0;
        Scanner input = new Scanner(System.in);

        System.out.println("1. Program creates array size at run time");
        System.out.println("2. Program displays array size");
        System.out.println("3. Program rounds sum and average of numbers to 2 decimal places");
        System.out.println("4. Numbers must be float data types, not double");

        //determine the size and construct the array
        System.out.print("\nEnter array size: ");
         size= input.nextInt();

        double[] myArray =  new double[size];

        System.out.println();

        //input the data
        for (int index=0; index < myArray.length; index++)
        {
           System.out.print("Enter num " + (index+1) + ": ");
           myArray[index] = input.nextDouble();
         }

        //sum values
        for (int i=0; i < myArray.length; i++)
        sum += myArray[i];

        //average values
        average = sum/size;

        //display sum and average
        System.out.printf("\nSum: " + "%.2f", sum);
        System.out.printf("\nAverage: " + "%.2f", average);

    }
}